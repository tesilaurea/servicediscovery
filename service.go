package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	gocql "github.com/gocql/gocql"
	"io/ioutil"
	"net/http"
)

func RegisterToClusterMonitor(metapod MetaPod) bool {
	fmt.Printf("Mi sto registrando presso il master\n")
	fmt.Printf("Controllo che non sono già registrato\n")
	confirm, id, uid := AlreadyRegistrated(metapod)
	if confirm == true {
		metapod.UUid = id
		_, err := http.Post("http://"+pathToClusterMonitorUpdate+metapod.Type, "application/json; charset=UTF-8",
			bytes.NewBuffer(StructToJson(metapod)))
		if err != nil {
			// handle error
			panic(err)
			return false
		}
		var microservices Microservices
		microservices.Deployment = deployment
		microservices.Type = service
		microservices.Uid = uid
		microservices.MicroserviceList = getMicroservice()
		fmt.Println(microservices)
		_, err = http.Post("http://"+pathToClusterMonitorMicroservicesUpdate, "application/json; charset=UTF-8",
			bytes.NewBuffer(StructToJson(microservices)))
		if err != nil {
			// handle error
			panic(err)
			return false
		}

		fmt.Printf("Ho aggiornato la mia info presso il master\n")
		return true
	}
	resp, err := http.Post("http://"+pathToClusterMonitorRegister+metapod.Type, "application/json; charset=UTF-8",
		bytes.NewBuffer(StructToJson(metapod)))
	if err != nil {
		// handle error
		panic(err)
		return false
	}
	body, _ := ioutil.ReadAll(resp.Body)
	var m MetaPod
	if err := json.Unmarshal(body, &m); err != nil {
		// w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		//w.WriteHeader(422) // unprocessable entity
		//if err := json.NewEncoder(w).Encode(err); err != nil {
		panic(err)
		return false
	}

	var microservices Microservices
	microservices.Deployment = deployment
	microservices.Type = service
	microservices.Uid = m.Uid
	microservices.MicroserviceList = getMicroservice()
	fmt.Println(microservices)
	_, err = http.Post("http://"+pathToClusterMonitorMicroservicesRegister, "application/json; charset=UTF-8",
		bytes.NewBuffer(StructToJson(microservices)))
	if err != nil {
		// handle error
		panic(err)
		return false
	}
	fmt.Printf("Mi sono registrato presso il master\n")
	return true
}

func AlreadyRegistrated(metapod MetaPod) (bool, gocql.UUID, string) {
	var re RegistratedEntity
	resp, err := http.Get("http://" + pathToClusterMonitorControl + metapod.Deployment)
	if err != nil {
		// handle error
		panic(err)
	}
	fmt.Println("http://" + pathToClusterMonitorControl + metapod.Deployment)
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}
	if err := json.Unmarshal(body, &re); err != nil {
		panic(err)
	}
	return re.Response, re.UUid, re.Uid
}
