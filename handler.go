package main

import (
	"encoding/json"
	"fmt"
	_ "github.com/gorilla/mux"
	_ "io"
	_ "io/ioutil"
	// metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	_ "k8s.io/client-go/kubernetes"
	_ "k8s.io/client-go/rest"
	_ "log"
	"net/http"
)

func GetHello(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Hello!")
}

func GetPodInfo(w http.ResponseWriter, r *http.Request) {

	var infoPod InfoPod
	infoPod.Hostname = hostname
	infoPod.NodeName = nodeName
	infoPod.PodIp = podIp
	infoPod.PodName = podName
	infoPod.PodNamespace = podNamespace
	infoPod.PodServiceAccount = podServiceAccount
	infoPod.Service = service
	infoPod.Deployment = deployment

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(infoPod); err != nil {
		panic(err)
	}
}

func GetMicroservices(w http.ResponseWriter, r *http.Request) {

	microservices := getMicroservice()
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(microservices); err != nil {
		panic(err)
	}
}

/*func ReceiveInfoPod(w http.ResponseWriter, r *http.Request) {
	// list pods
	pods, err := client.CoreV1().Pods("").List(metav1.ListOptions{})
	if err != nil {
		log.Fatal("Failed to retrieve pods")
		//return
	}
	for _, p := range pods.Items {
		log.Fatal("Found pods: %s/%s", p.Namespace, p.Name)
	}

}*/
