package main

type InfoPod struct {
	PodName           string `json:"podname"`
	Hostname          string `json:"hostname"`
	PodIp             string `json:"podip"`
	NodeName          string `json:"nodename"`
	PodNamespace      string `json:"podnamespace"`
	PodServiceAccount string `json:"podserviceaccount"`
	Service           string `json:"service"`
	Deployment        string `json:"deployment"`
}
