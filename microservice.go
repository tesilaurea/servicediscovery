package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

type Microservice struct {
	Name string `json:"name"`
	Tu   int    `json:"tu"`
}

func getMicroservice() []Microservice {
	raw, err := ioutil.ReadFile("./pages.json")
	if err != nil {
		fmt.Println(err.Error())
	}

	var c []Microservice
	json.Unmarshal(raw, &c)
	return c
}

type Microservices struct {
	Uid              string         `json:"uid"`
	Type             string         `json:"type"`
	Deployment       string         `json:"deployment"`
	MicroserviceList []Microservice `json:"microservices"`
}
