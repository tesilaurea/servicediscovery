package main

import (
	_ "flag"
	"fmt"
	"github.com/gorilla/handlers"
	// metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	_ "k8s.io/client-go/tools/clientcmd"
	"log"
	"net/http"
	"os"
)

func main() {
	var port = ":8082"
	fmt.Println("Versione nuova")
	podName = os.Getenv("MY_POD_NAME")
	hostname = os.Getenv("HOSTNAME")
	podIp = os.Getenv("MY_POD_IP")
	nodeName = os.Getenv("MY_NODE_NAME")
	podNamespace = os.Getenv("MY_POD_NAMESPACE")
	podServiceAccount = os.Getenv("MY_POD_SERVICE_ACCOUNT")

	service = os.Getenv("MY_POD_MICROSERVICE")
	deployment = os.Getenv("MY_POD_DEPLOYMENT")

	pathToClusterMonitorRegister = os.Getenv("CLUSTER_MONITOR_HOST") + "/register/"
	pathToClusterMonitorUpdate = os.Getenv("CLUSTER_MONITOR_HOST") + "/register/update/"
	pathToClusterMonitorControl = os.Getenv("CLUSTER_MONITOR_HOST") + "/register/confirm/"
	pathToClusterMonitorMicroservicesRegister = os.Getenv("CLUSTER_MONITOR_HOST") + "/microservices/register"
	pathToClusterMonitorMicroservicesUpdate = os.Getenv("CLUSTER_MONITOR_HOST") + "/microservices/update"
	fmt.Println(pathToClusterMonitorRegister)
	fmt.Println(pathToClusterMonitorUpdate)
	fmt.Println(pathToClusterMonitorControl)
	fmt.Println(pathToClusterMonitorMicroservicesRegister)
	fmt.Println(pathToClusterMonitorMicroservicesUpdate)

	var metaPod MetaPod
	metaPod.Type = service
	metaPod.Ip = podIp
	metaPod.Node = nodeName
	metaPod.Deployment = deployment

	fmt.Printf("Microservice controller start\n")

	// creates the in-cluster config
	config, err := rest.InClusterConfig()
	if err != nil {
		panic(err.Error())
	}
	// creates the clientset
	client, err = kubernetes.NewForConfig(config)
	if err != nil {
		panic(err.Error())
	}

	registrated = RegisterToClusterMonitor(metaPod)
	router := NewRouter()
	log.Fatal(http.ListenAndServe(port, handlers.CORS()(router)))
}
