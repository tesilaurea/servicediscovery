package main

import "net/http"

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type Routes []Route

/**
 * Endoint dell'applicazione, con i relativi metodi che vengono invocati
 *
 */

var routes = Routes{
	/*	Route{
		"ReceiveInfoPod",
		"GET",
		"/receiveInfoPod",
		ReceiveInfoPod,
	},*/
	Route{
		"GetHello",
		"GET",
		"/getHello",
		GetHello,
	},
	Route{
		"GetPodInfo",
		"GET",
		"/getPodInfo",
		GetPodInfo,
	},
	Route{
		"GetMicroservices",
		"GET",
		"/getMicroservices",
		GetMicroservices,
	},
}
